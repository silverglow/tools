#!/bin/bash

# The purpose of this script is to create the necessary roles and role bindings that allow
# a tiller to be installed in a single namespace rather than globally.
# The script will also optionally grant access to secondary namespaces

#The service account that this tiller instance will use
SERVICE_ACCOUNT="tiller"

#The name space in which the service account will be created
SERVICE_ACCOUNT_NAMESPACE="sa-test-namespace"

#The name of the roles that will be created for this tiller instance
TILLER_ROLE_NAME="tiller-manager"

#Extra namespace to which this tiller will have access
ACCESS_TO_ADDITIONAL_NAMESPACE+=("additional-namespace-1")
#ACCESS_TO_ADDITIONAL_NAMESPACE+=("additional-namespace-2")
#ACCESS_TO_ADDITIONAL_NAMESPACE+=("additional-namespace-3")

function check_connectivity_to_cluster {
  if (kubectl auth can-i create namespace  >& /dev/null ); then
    return 0
  else
    return 1
  fi
}


function create_namespace {
  #Create a namespace if it doesn't already exist
  local lcl_NAMESPACE=$1

  PREEXISTING_NAMESPACE=$(kubectl get namespace -o json| jq -r ".items[] | select(.metadata.name | match(\"${lcl_NAMESPACE}\")) | .metadata.name")

  if [[ -n ${PREEXISTING_NAMESPACE} ]]; then
    echo "= Namespace ${lcl_NAMESPACE} already exists. Won't try to recreate"
  else
    echo "+ Creating namespace ${lcl_NAMESPACE}"
    cat <<EOF | kubectl create namespace ${lcl_NAMESPACE}
EOF
  fi
}

function create_sa_account {
  #Create a SA account in a namespace if it doesn't already exist
  local lcl_NAMESPACE=$1
  local lcl_TILLER_ACCOUNT_NAME=$2
  
  #Get a list of the accounts with the target name. The output should either be empty or contain a single name if the service account already exists
  PREEXISTING_SERVICEACCOUNT=$(kubectl get serviceaccount -n ${lcl_NAMESPACE} -o json | jq -r ".items[] | select(.metadata.name | match(\"${lcl_TILLER_ACCOUNT_NAME}\")) | .metadata.name")
  if [[ -n  ${PREEXISTING_SERVICEACCOUNT} ]]; then
    echo "= Serivce account ${PREEXISTING_SERVICEACCOUNT} in namespace ${lcl_TILLER_ACCOUNT_NAME} already exists. Won't try to recreate"
  else
    echo "+ Creating service account ${PREEXISTING_SERVICEACCOUNT} in namespace ${lcl_TILLER_ACCOUNT_NAME}"
    cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: ${lcl_TILLER_ACCOUNT_NAME}
  namespace: ${lcl_NAMESPACE}
EOF
fi
}

function create_namespace_bound_role {
  #Create a role in the specified namespace
  local lcl_NAMESPACE=$1
  local lcl_ROLE_NAME=$2
  echo "+ Creating role ${lcl_ROLE_NAME} in namespace ${lcl_NAMESPACE}"
  cat <<EOF | kubectl create -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: Role
metadata:
  name: ${lcl_ROLE_NAME}
  namespace: ${lcl_NAMESPACE}
rules:
- apiGroups: ["", "batch", "extensions", "apps"]
  resources: ["*"]
  verbs: ["*"]
EOF
}

function review_preexisting_roles {
  #Clean up preexisting roles and rolebinding from the namespaces, to ensure that what you have after the script runs is exactly what is described in the script
  local lcl_NAMESPACE=$1
  #Depending on the case statement below action can either be "report" - to just report there are preexisting roles and/or rolebindings or delete - to delete if any are found
  local lcl_ACTION=${2:-"delete"}

  local lcl_BLACKLISTED_NAMESPACES="default|external-dns|kube-public|kube-system|nginx-ingress|e2e-test"
  if [[ ${lcl_NAMESPACE} =~ ^(${lcl_BLACKLISTED_NAMESPACES})$ ]]; then
    echo "Exiting. This script will not process namespaces from the following list: ${lcl_BLACKLISTED_NAMESPACES}"
    exit 1
  fi

  PREEXISTING_ROLES=$(kubectl get role -n ${lcl_NAMESPACE} -o json | jq -r .items[].metadata.name)
  PREEXISTING_ROLEBINDING=$(kubectl get rolebinding -n ${lcl_NAMESPACE} -o json | jq -r .items[].metadata.name)

    case $lcl_ACTION in
      report)
        if [[ -n ${PREEXISTING_ROLES} ]]; then echo "There are preexisting roles in the namespace ${lcl_NAMESPACE}"; fi
        if [[ -n ${PREEXISTING_ROLEBINDING} ]]; then echo "There are preexisting role bindings in the namespace ${lcl_NAMESPACE}"; fi
      ;;
      delete)
        if [[ -n ${PREEXISTING_ROLES} ]]; then
          for ROLE_TO_DELETE in ${PREEXISTING_ROLES}; do
            echo "- Deleting role ${ROLE_TO_DELETE} from namespace ${lcl_NAMESPACE}"
            kubectl delete role ${ROLE_TO_DELETE} -n ${lcl_NAMESPACE}
          done
        fi
        if [[ -n ${PREEXISTING_ROLEBINDING} ]]; then 
          for ROLEBINDING_TO_DELETE in ${PREEXISTING_ROLEBINDING}; do
            echo "- Deleting rolebinding ${ROLEBINDING_TO_DELETE} from namespace ${lcl_NAMESPACE}"
            kubectl delete rolebinding ${ROLEBINDING_TO_DELETE} -n ${lcl_NAMESPACE}
          done
        fi
      ;;
      *)
        echo "No cleanup action"
      ;;
    esac
}

function create_rolebinding {
  #Function to create rolebindings
  local lcl_SA_ACCOUNT=$1
  local lcl_SA_ACCOUNT_NAMESPACE=$2
  local lcl_ROLE_NAME=$3
  local lcl_ROLE_NAMESPACE=$4

  local lcl_ROLE_BINDING_NAME="${lcl_ROLE_NAME}_${lcl_ROLE_NAMESPACE}"
  echo "+ Creating role binding ${lcl_ROLE_NAME}_${lcl_ROLE_NAMESPACE} in namespace ${lcl_ROLE_NAMESPACE}"
  cat <<EOF | kubectl create -f -
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: RoleBinding
metadata:
  name: ${lcl_ROLE_BINDING_NAME}
  namespace: ${lcl_SA_ACCOUNT_NAMESPACE}
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: ${lcl_ROLE_BINDING_NAME}
subjects:
  - kind: ServiceAccount
    name: ${lcl_SA_ACCOUNT_NAMESPACE}
    namespace: ${lcl_SA_ACCOUNT_NAMESPACE}
EOF
}

if ! (check_connectivity_to_cluster); then
  echo "There is no connectivity to the cluster. Exiting."
  exit 1
fi

#Create namespace, service account, roles and rolebindings (and clean up old ones) for the main namespace
create_namespace "${SERVICE_ACCOUNT_NAMESPACE}"
create_sa_account "${SERVICE_ACCOUNT_NAMESPACE}" "${SERVICE_ACCOUNT}"
review_preexisting_roles "${SERVICE_ACCOUNT_NAMESPACE}" "delete"
create_namespace_bound_role "${SERVICE_ACCOUNT_NAMESPACE}" "${TILLER_ROLE_NAME}"
create_rolebinding "${SERVICE_ACCOUNT}" "${SERVICE_ACCOUNT_NAMESPACE}" "${TILLER_ROLE_NAME}" "${SERVICE_ACCOUNT_NAMESPACE}"

#Create namespace, service account, roles and rolebindings (and clean up old ones) for the additional namespaces
for ADDITONAL_NAMESPACE in ${ACCESS_TO_ADDITIONAL_NAMESPACE[@]}; do
  create_namespace ${ADDITONAL_NAMESPACE}
  create_sa_account "${ADDITONAL_NAMESPACE}" "${SERVICE_ACCOUNT}"
  review_preexisting_roles "${ADDITONAL_NAMESPACE}" "delete"
  create_namespace_bound_role "${ADDITONAL_NAMESPACE}" "${TILLER_ROLE_NAME}"
  create_rolebinding "${SERVICE_ACCOUNT}" "${SERVICE_ACCOUNT_NAMESPACE}" "${TILLER_ROLE_NAME}" "${ADDITONAL_NAMESPACE}"
done
