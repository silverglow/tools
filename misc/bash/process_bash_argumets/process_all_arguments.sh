#!/bin/bash

#Expected parameter format - basically alphanum:alphanum
PARAM_REGEX=^[a-zA-Z0-9_-]+:[a-zA-Z0-9_-]+$

#Iterrate over the the parameters array for matching key-value pairs
for PARAMETER in "${@}"; do
  if [[ "${PARAMETER}" =~ ${PARAM_REGEX} ]]; then
    IFS=: read -r PARAM_NAME PARAM_VALUE <<< ${PARAMETER}
    eval "${PARAM_NAME}=${PARAM_VALUE}"
fi
done

function param_expansion {
  #This will echo commands that if executed (with eval) will set the variables found in the input string
  local lcl_PARAM_STRING=$1
  for PARAMETER in "${@}"; do
  if [[ "${PARAMETER}" =~ ${PARAM_REGEX} ]]; then
    IFS=: read -r PARAM_NAME PARAM_VALUE <<< ${PARAMETER}
    echo "${PARAM_NAME}=${PARAM_VALUE}"
  fi
done
}

function param_expansion_local_variables {
  #This will echo commands that if executed (with eval) will set the variables found in the input string as local variables
  local lcl_PARAM_STRING=$1
  for PARAMETER in "${@}"; do
  if [[ "${PARAMETER}" =~ ${PARAM_REGEX} ]]; then
    IFS=: read -r PARAM_NAME PARAM_VALUE <<< ${PARAMETER}
    echo "local ${PARAM_NAME}=${PARAM_VALUE}"
  fi
done
}

#Set execute the variable setting commands in the main string
eval $(param_expansion ${@})

#Execute the variable setting commands inside a function
function get_column_separated_variables_in_function {
  eval $(param_expansion_local_variables ${@})
}
