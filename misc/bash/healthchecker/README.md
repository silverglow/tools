## Healthcheck script

### Description
The main script pre-defines a set of healthchecks. The healthcheck targets and the various connection parameters are configured via a property file. The idea is that there would be a dedicated config generation script for each environment that will be monitored. The config generator script would gather information from the same place the application would (like the application config files, docker/kubernetes environment variables etc) and generate a config file for the main healthcheck script. Ideally they would execute sequentially each time to ensure that the healthchecks dynamically respond to environment changes

### Config file

Currently the healthcheck script expect it's config file to be in the same folder as the script and be called liveness_check_config.conf

#### Config file format
Currently the healtheck configuration file expects one or more line with the following format
```
CHECKS_TO_DO+=("Type_of_check;client;username;password;check_target;healthy_criteria;healty_message;unhealthy_message
```

`type_of_check`            = HTTP_STATUS|MONGO_DB_EXISTS  
`client`  
* for `HTTP_STATUS`      = curl|wget  
* for `MONGO_DB_EXISTS`  = mongo  

`username`                 = username_string|*EMPTY*  
`password`                 = password_string|*EMPTY*  
`check_target`             = url_to_system_to_be_checked  
`healthy_criteria`  
* for `HTTP_STATUS`      = *BASH_STRING_MATCH_REGULAR_EXPRESSION*  (http://wiki.bash-hackers.org/syntax/pattern)  
* for `MONGO_DB_EXISTS`  = field_not_used  

`healthy_message`          = string|*EMPTY*  
`unhealthy_message`        = string|*EMPTY*  

#### A few examples
./liveness_check_config.conf
```
CHECKS_TO_DO+=("HTTP_STATUS;curl;;;https://httpstat.us/201;;")
CHECKS_TO_DO+=("HTTP_STATUS;wget;-nousername-;-nopassword-;https://httpstat.us/301;([1-3][0-9][0-9])")
CHECKS_TO_DO+=("HTTP_STATUS;curl;-nousername-;-nopassword-;https://httpstat.us/202;${DEFAULT_HTTP_SUCCESS_CODES_REGEX};")
CHECKS_TO_DO+=("HTTP_STATUS;curl;-nousername-;-nopassword-;https://httpstat.us/301;([1-3][0-9][0-9]);OK000001 Connected to https://httpstat.us;BAD000001 Unsuccesful connection to https://httpstat.us;")
CHECKS_TO_DO+=("MONGO_DB_EXISTS;mongo;-nousername-;-nopassword-;127.0.0.1:27017/test;-none-;OK000002 Mongo/test;BAD000001 mongo/test;")
```

### Invoking the healthchecks in kubernetes

The original purpose of these healthchecks was periodically execute them as either a kubernetes livenessProbe or readinessProbe. To to that you would append this code to the pod specification within the kubernetes deployment. There are additional kubernetes pramaters that can pe specified (initialDelaySeconds, periodSeconds, timeoutSeconds, successThreshold, failureThreshold....). For more information check the kubernetes documentation https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-probes/


The readiness probe would restart your pod if the healthcheck fails
```
readinessProbe:
  exec:
    command:
    - /bin/sh
    - "/healthcheck_scripts/beekeeper_server_hlt_conf_gen.sh; /healthcheck_scripts/healthcheck_commando.sh"
  initialDelaySeconds: 60
  periodSeconds: 15
```

The liveness probe will just prevent traffic from reaching the pod if the healthcheck fails
```
livenessProbe:
  exec:
    command:
    - /bin/sh
    - "/healthcheck_scripts/beekeeper_server_hlt_conf_gen.sh; /healthcheck_scripts/healthcheck_commando.sh"
  initialDelaySeconds: 60
  periodSeconds: 15
```

### Config file generators
Currently there is a folder called config_generators that would contain various config generator script.
