#!/bin/bash

# Dependencies: Depending on the checks you plan to do the script required one or more of the following - mongo client, curl, wget

#This server will mostly be utilized automaticaly so parameters will be provided in a comma separated string rather than command line options. The format is:
# CHECKS_TO_DO+=("Type_of_check;client;username;password;check_target;healthy_criteria;healty_message;unhealthy_message
#The configuration is meant to be provided via a configuration file that will be sourced
#Sample entries for the config file
# CHECKS_TO_DO+=("HTTP_STATUS;curl;-nousername-;-nopassword-;https://httpstat.us/301;([1-3][0-9][0-9]);OK000001 Connected to https://httpstat.us;BAD000001 Unsuccesful connection to https://httpstat.us;")
# CHECKS_TO_DO+=("HTTP_STATUS;curl;;;https://httpstat.us/301;([1-3][0-9][0-9]);OK000001 Connected to https://httpstat.us;BAD000001 Unsuccesful connection to https://httpstat.us;")
# CHECKS_TO_DO+=("HTTP_STATUS;wget;username;password;https://httpstat.us/301;([1-3][0-9][0-9])")

# type_of_check            = HTTP_STATUS|MONGO_DB_EXISTS
# client
#   - for HTTP_STATUS      = curl|wget
#   - for MONGO_DB_EXISTS  = mongo
# username                 = username_string|*EMPTY*
# password                 = password_string|*EMPTY*
# check_target             = url_to_system_to_be_checked
# healthy_criteria
#   - for HTTP_STATUS      = *BASH_STRING_MATCH_REGULAR_EXPRESSION*  (http://wiki.bash-hackers.org/syntax/pattern)
#   - for MONGO_DB_EXISTS  = field_not_used
# healthy_message          = string|*EMPTY*
# unhealthy_message        = string|*EMPTY*


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
#This config file can/should contain the healthcheck targets
source ${DIR}/liveness_check_config.conf

#The default http client
DEFAULT_HTTP_CLIENT="curl"
DEFAULT_HTTP_SUCCESS_CODES_REGEX="([1-3][0-9][0-9])"

DEFAULT_MONGO_CLIENT="mongo"

#If any function changes this to true the script will exit with an error code
A_HEALTHCHECK_FAILED="false"

CHECKS_TO_DO+=("HTTP_STATUS;curl;;;https://httpstat.us/201;;")
CHECKS_TO_DO+=("HTTP_STATUS;wget;-nousername-;-nopassword-;https://httpstat.us/301;([1-3][0-9][0-9])")
CHECKS_TO_DO+=("HTTP_STATUS;curl;-nousername-;-nopassword-;https://httpstat.us/202;${DEFAULT_HTTP_SUCCESS_CODES_REGEX};")
CHECKS_TO_DO+=("HTTP_STATUS;curl;-nousername-;-nopassword-;https://httpstat.us/301;([1-3][0-9][0-9]);OK000001 Connected to https://httpstat.us;BAD000001 Unsuccesful connection to https://httpstat.us;")
CHECKS_TO_DO+=("MONGO_DB_EXISTS;mongo;-nousername-;-nopassword-;127.0.0.1:27017/test;-none-;OK000002 Mongo/test;BAD000001 mongo/test;")

function http_response_with_curl {
  local lcl_RESPONSE_CODE=""
  local lcl_URL_TO_CHECK="$1"

  lcl_RESPONSE_CODE=$(curl -o /dev/null --silent --head --write-out '%{http_code}\n' ${lcl_URL_TO_CHECK} | cut -d\  -f1) || true
  echo ${lcl_RESPONSE_CODE}
}

function http_response_with_wget {
  local lcl_RESPONSE_CODE=""
  local lcl_URL_TO_CHECK="$1"

  lcl_RESPONSE_CODE=$(wget --max-redirect 0 -qO- --server-response ${lcl_URL_TO_CHECK} 2>&1 | grep HTTP/.*\ [0-9][0-9][0-9]\ .* | sed "s/^\ *//" | cut -d\  -f2) || true
  #Mimic curl and return 000 in case of an error
  lcl_RESPONSE_CODE=${lcl_RESPONSE_CODE:-"000"}
  echo ${lcl_RESPONSE_CODE}
}

function evaulate_http_response {
  local lcl_RESPONSE_CODE="$1"
  local lcl_SUCCESS_CODES_REGEX="${2:-$DEFAULT_HTTP_SUCCESS_CODES_REGEX}"
  local lcl_HEALTHCHECK_OK_MESSAGE="$3"
  local lcl_HEALTHCHECK_BAD_MESSAGE="$4"

  if [[ ${lcl_RESPONSE_CODE} =~ ${lcl_SUCCESS_CODES_REGEX} ]]; then
    echo "${lcl_HEALTHCHECK_OK_MESSAGE:-$lcl_RESPONSE_CODE}"
  else
    A_HEALTHCHECK_FAILED="true"
    echo "${lcl_HEALTHCHECK_BAD_MESSAGE:-$lcl_RESPONSE_CODE}"
    
  fi
}

function mongo_return_database_index {
  #Apparently the following command retunrs the index of a database within Mongo. If the value is lower than zero then there is no table under this name
  local lcl_MONGO_HOST="$1"
  local lcl_MONGO_PORT="$2"
  local lcl_MONGO_DATABASE="$3"
  local lcl_MONGO_USER="$4"
  local lcl_MONGO_PASSWORD="$5"

  local lcl_MONGO_PORT_STRING=${lcl_MONGO_PORT:+":$lcl_MONGO_PORT"}
  local lcl_MONGO_DATABASE_STRING=${lcl_MONGO_DATABASE:+":/$lcl_MONGO_DATABASE"}
  local lcl_MONGO_USER_STRING=${lcl_MONGO_USER:+"-u $lcl_MONGO_USER"}
  local lcl_MONGO_PASSWORD_STRING=${lcl_MONGO_PASSWORD:+"-p $lcl_MONGO_PASSWORD"}

  local lcl_MONGO_CONNECT_STRING="${DEFAULT_MONGO_CLIENT} --quiet ${lcl_MONGO_USER_STRING} ${lcl_MONGO_PASSWORD_STRING} ${lcl_MONGO_HOST}${lcl_MONGO_PORT_STRING}\
    --eval \"db.getMongo().getDBNames().indexOf('${lcl_MONGO_DATABASE}')\""
  # The extra parts "2>/dev/null || echo "-101") | tail -n 1" are meant to supress the error output generated when the host is unreachable. Instead just an error "-101" is returned
  # eval executed the string witin the lcl_MONGO_CONNECT_STRING variable as a command, then it is passed onwards to the rest of the command
  local lcl_MONGO_DB_INDEX=$((eval ${lcl_MONGO_CONNECT_STRING} 2>/dev/null || echo "-101") | tail -n 1 )

  if [[ ${lcl_MONGO_DB_INDEX} -gt 0 ]]; then
    echo "DB_EXISTS"
  fi
}

function evaluate_mongo_response {
  local lcl_MONGO_DB_STATUS="$1"
  local lcl_MONGO_DATABASE="$2"
  local lcl_MONGO_HEALTHY_CRITERIA="$3"
  local lcl_HEALTHCHECK_OK_MESSAGE="$4"
  local lcl_HEALTHCHECK_BAD_MESSAGE="$5"

  case ${lcl_MONGO_DB_STATUS} in
  DB_EXISTS)
  echo "${lcl_HEALTHCHECK_OK_MESSAGE}"
    ;;
  *)
  A_HEALTHCHECK_FAILED="true"
  echo "${lcl_HEALTHCHECK_BAD_MESSAGE}"
    ;; 
  esac
}


#Bad stuff will happen without the quotes at places where they might seem excessive. Don't experiment with removing them unless you want to feel like Neo in the Matrix
#Go execute all of the tests sequentially
for SINGLE_HEALTH_CHECK_STRING in "${CHECKS_TO_DO[@]}"; do
  #Extract the parameters from the comma separated strings
  HEALTHCHECK_METOD=$(cut -d\; -f1 <<< ${SINGLE_HEALTH_CHECK_STRING})
  HEALTHCHECK_CLIENT=$(cut -d\; -f2 <<< ${SINGLE_HEALTH_CHECK_STRING})
  HEALTHCHECK_USERNAME=$(cut -d\; -f3 <<< ${SINGLE_HEALTH_CHECK_STRING})
  # This gives you to explicitly state that no username will be used. Optionally just leave the username field empty
  if [[ ${HEALTHCHECK_USERNAME} == '-nousername-' ]]; then HEALTHCHECK_USERNAME=""; fi
  HEALTHCHECK_PASSWORD=$(cut -d\; -f4 <<< ${SINGLE_HEALTH_CHECK_STRING})
  # This gives you to explicitly state that no password will be used. Optionally just leave the username field empty
  if [[ ${HEALTHCHECK_PASSWORD} == '-nopassword-' ]]; then HEALTHCHECK_PASSWORD=""; fi
  #Set the default http client
  HEALTHCHECK_URL=$(cut -d\; -f5 <<< ${SINGLE_HEALTH_CHECK_STRING})
  HEALTHCHECK_HEALTHY_CRITERIA=$(cut -d\; -f6 <<< ${SINGLE_HEALTH_CHECK_STRING})
  HEALTHCHECK_OK_MESSAGE=$(cut -d\; -f7 <<< ${SINGLE_HEALTH_CHECK_STRING})
  HEALTHCHECK_BAD_MESSAGE=$(cut -d\; -f8 <<< ${SINGLE_HEALTH_CHECK_STRING})

  #Call the correct healtheck function depending on the healthcheck method
  case ${HEALTHCHECK_METOD} in
    HTTP_STATUS)
      HEALTHCHECK_CLIENT=${HEALTHCHECK_CLIENT:-$DEFAULT_HTTP_CLIENT}
      HEALTHCHECK_HEALTHY_CRITERIA=${HEALTHCHECK_HEALTHY_CRITERIA:-$DEFAULT_HTTP_SUCCESS_CODES_REGEX}
      if [[ ${HEALTHCHECK_CLIENT} == "curl" ]]; then
        CHECK_FUNCTION="http_response_with_curl"
      elif [[ ${HEALTHCHECK_CLIENT} == "wget" ]]; then
        CHECK_FUNCTION="http_response_with_wget"
      else
        echo "Unknown healthcheck client/method '${HEALTHCHECK_METOD}'."
      fi 
      HEALTHCHECK_RESPONSE=$(${CHECK_FUNCTION} "${HEALTHCHECK_URL}")
      evaulate_http_response "${HEALTHCHECK_RESPONSE}" "${HEALTHCHECK_HEALTHY_CRITERIA}" "${HEALTHCHECK_OK_MESSAGE}" "${HEALTHCHECK_BAD_MESSAGE}"
    ;;
    MONGO_DB_EXISTS)
    HEALTHCHECK_CLIENT=${HEALTHCHECK_CLIENT:-$DEFAULT_MONGO_CLIENT}
    MONGO_URL=${HEALTHCHECK_URL%:*}
    MONGO_PORT=$(cut -d/ -f1 <<< ${HEALTHCHECK_URL##*:})
    MONGO_DB=$(cut -d/ -f2 <<< ${HEALTHCHECK_URL##*:})

    DOES_MONGO_DB_EXIST=$(mongo_return_database_index ${MONGO_URL} ${MONGO_PORT} ${MONGO_DB} ${HEALTHCHECK_USERNAME} ${HEALTHCHECK_PASSWORD})
    evaluate_mongo_response "${DOES_MONGO_DB_EXIST}" "${MONGO_DB}" "${HEALTHCHECK_HEALTHY_CRITERIA}" "${HEALTHCHECK_OK_MESSAGE}" "${HEALTHCHECK_BAD_MESSAGE}"
    ;;
    *)
      echo "Unknown check method"
    ;;
  esac
done

#The idea is to go through all healthecks before exitting with an error. In this way you can see all logs
if [[ "${A_HEALTHCHECK_FAILED}" == "true" ]]; then
  exit 1
else
  exit 0
fi
