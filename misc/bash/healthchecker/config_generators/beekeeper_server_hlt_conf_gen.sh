#!/bin/bash
#Based on https://gitlab.cwscloud.net/design/beekeeper-server/blob/develop/README.md

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DEFAULT_APP_CONFIG_FILE_LOCATION="/config/common.json"

DEFAULT_HEALTHCHECK_METOD="HTTP_STATUS"
DEFAULT_HEALTHCHECK_CLIENT="curl"
DEFAULT_HEALTHCHECK_HEALTHY_CRITERIA="([1-3][0-9][0-9])"
DEFAULT_HEALTHCHECK_OK_MESSAGE="Service ${SERVICE_BEING_CHECKED} OK"
DEFAULT_HEALTHCHECK_BAD_MESSAGE="Serivce ${SERVICE_BEING_CHECKED} BAD"

DEFAULT_GENERATED_CONFIG_LOCATION=${DIR}/liveness_check_config.conf

#You could overwrite the actual parameter buy setting the "CL_APP_CONFIG_FILE_LOCATION" variable with the desired value
APP_CONFIG_FILE_LOCATION="${CL_APP_CONFIG_FILE_LOCATION:-$DEFAULT_APP_CONFIG_FILE_LOCATION}"

#The config file is actuall javascript, not json. The following command converts it to json
#APP_CONFIG_JSON=$(node -e "console.log(JSON.stringify(require(\"${APP_CONFIG_FILE_LOCATION}\")))")
APP_CONFIG_JSON=$(cat ${APP_CONFIG_FILE_LOCATION})


#The square brackets are used because JQ appears to treat the dashes as special symbols
AUTH_URL="$(jq --raw-output .common.auth.url  <<< $APP_CONFIG_JSON )/health"
DOTCMS_URL="$(jq --raw-output .[\"journey-designer-server\"].connections.dotCms.baseUri   <<< $APP_CONFIG_JSON )"
MONGO_HOST_URL="$(jq --raw-output .common.connections.mongo.host   <<< $APP_CONFIG_JSON )"
MONGO_PORT="$(jq --raw-output .common.connections.mongo.port   <<< $APP_CONFIG_JSON )"
MONGO_DATABASE="$(jq --raw-output .common.connections.mongo.database   <<< $APP_CONFIG_JSON )"
GIT_BASE_URL="$(jq --raw-output .[\"journey-designer-server\"].gitPublisher.baseUrl  <<< $APP_CONFIG_JSON )"
GIT_USERNAME="$(jq --raw-output .[\"journey-designer-server\"].gitPublisher.username  <<< $APP_CONFIG_JSON )"
GIT_PASSWORD="$(jq --raw-output .[\"journey-designer-server\"].gitPublisher.password  <<< $APP_CONFIG_JSON )"

if [ -f ${DEFAULT_GENERATED_CONFIG_LOCATION} ]; then rm ${DEFAULT_GENERATED_CONFIG_LOCATION}; fi

SERVICE_BEING_CHECKED=${AUTH_URL}
echo "CHECKS_TO_DO+=(\"${DEFAULT_HEALTHCHECK_METOD};${DEFAULT_HEALTHCHECK_CLIENT};-nousername-;-nopassword-;;${SERVICE_BEING_CHECKED};${DEFAULT_HEALTHCHECK_HEALTHY_CRITERIA};HEALTHCHECK OK ${SERVICE_BEING_CHECKED};HEALTHCHECK BAD ${SERVICE_BEING_CHECKED};\")" >> ${DEFAULT_GENERATED_CONFIG_LOCATION}

SERVICE_BEING_CHECKED="${DOTCMS_URL}/dotAdmin"
echo "CHECKS_TO_DO+=(\"${DEFAULT_HEALTHCHECK_METOD};${DEFAULT_HEALTHCHECK_CLIENT};-nousername-;-nopassword-;${SERVICE_BEING_CHECKED};${DEFAULT_HEALTHCHECK_HEALTHY_CRITERIA};HEALTHCHECK OK ${SERVICE_BEING_CHECKED};HEALTHCHECK BAD ${SERVICE_BEING_CHECKED};\")" >> ${DEFAULT_GENERATED_CONFIG_LOCATION}

SERVICE_BEING_CHECKED="${MONGO_HOST_URL}:${MONGO_PORT}/${MONGO_DATABASE}"
echo "CHECKS_TO_DO+=(\"MONGO_DB_EXISTS;mongo;-nousername-;-nopassword-;${SERVICE_BEING_CHECKED};--none--;HEALTHCHECK OK ${SERVICE_BEING_CHECKED};HEALTHCHECK BAD ${SERVICE_BEING_CHECKED};\")" >> ${DEFAULT_GENERATED_CONFIG_LOCATION}
